# InstaMock
![Alt text](https://imgur.com/kBWI27e.jpg "Optional title")

## Introduction
InstaMock is a mock clone app that is being used as a demo as a part of a portfolio. It pulls data from a mock json file and uses a service layer to display posts to the user. Built using Swift 5 and MVVM design pattern.

## Current Features
#### Feed style view allows user to scroll through posts and for posts to be "liked"
![Alt text](https://imgur.com/T34PjI4.jpg "Optional title")
#### "View comments" button opens new view controller where comments are displayed and can be "liked"
![Alt text](https://imgur.com/tOGrpQ1.jpg "Optional title")

## Features in Progress
### UI is set up and ability to add new comment is currently being worked on
![Alt text](https://imgur.com/ltgMuIl.jpg "Optional title")

